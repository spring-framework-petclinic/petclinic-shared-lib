def call (){
    pipeline {
        agent {label 'maitre'}
        parameters {
            string(name:'TAG', description:'Saisie du tag:', trim: true)
            string(name:'HASH', defaultValue:'eadc31566795b81f947b99dda41af98f3fe05784', description:'Saisie du hash commit:', trim: true)
        }

        environment {
            // créer un crédential de type 'Secret Text' et utiliser l'identifiant
            TEAMS_WEBHOOK = credentials('teams-webhook')
        }

        stages {

            stage('tag') {
                steps {
                    sshagent(['904cc2da-b6c6-4820-af05-dcc4f4e3e3fe']) {
                        sh "git config --global user.email jenkins@jenkins"

                        sh "git config --global user.name Jenkins"

                        sh "git tag -a ${params.TAG} ${params.HASH} -m ${params.TAG}"

                        sh "git push origin ${params.TAG}"
                    }
                }
                post {
                    success {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: '239B56',
                        message: 'Step tag OK - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                    unsuccessful {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: 'CB4335',
                        message: 'Step tag KO - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                }
            }

            stage ('publish') {

                environment {

                    REGISTRY_AUTH = credentials('docker-hub-usr-pass')

                }

                steps {

                    sh './mvnw clean package jib:build -Dmaven.test.skip'

                }
                post {
                    success {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: '239B56',
                        message: 'Step publish OK - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                    unsuccessful {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: 'CB4335',
                        message: 'Step publish KO - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                }

            }
        }
        post {
            success {
                // One or more steps need to be included within each condition's block.
                office365ConnectorSend color: '239B56',
                message: 'Job OK - Matthieu', 
                webhookUrl: "$TEAMS_WEBHOOK"
            }
            unsuccessful {
                // One or more steps need to be included within each condition's block.
                office365ConnectorSend color: 'CB4335',
                message: 'Job KO - Matthieu', 
                webhookUrl: "$TEAMS_WEBHOOK"
                
            }
        }

    }
}