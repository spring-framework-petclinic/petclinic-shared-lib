pipeline {

    agent any

    parameters {

        string(name:'TAG', description:'Saisie du tag:')

        string(name:'HASH', description:'Saisie du hash commit:')

    }

    stages {

        stage('Creation TAG') {

            steps {

                //créer un tag qui porte le nom saisi

                sshagent(credentials: ['jenkins-rsa-private']) {



                   sh "git config --global user.email jenkins@jenkins"

                   sh "git config --global user.name Jenkins"

                   sh "git tag -a ${params.TAG} ${params.HASH} -m ${params.TAG}"

                   sh "git push origin ${params.TAG}"

                }      

            }

        }

        stage ('publish') {

            environment {

                REGISTRY_AUTH = credentials('docker-hub-usr-pass2')

            }

            steps {

                sh './mvnw clean package jib:build -Dmaven.test.skip'

            }

        }

    }

}