def call (){
    pipeline {
        agent any


        environment {
            // créer un crédential de type 'Secret Text' et utiliser l'identifiant
            TEAMS_WEBHOOK = credentials('teams-webhook')
        }

        stages {

            stage('Verify') {
                steps {
                    echo 'Verify'
                    sh'./mvnw -v'
                }
                post {
                    success {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: '239B56',
                        message: 'Step Verify OK - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                    unsuccessful {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: 'CB4335',
                        message: 'Step Verify KO - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                }
            }
            stage('Compile') {
                steps {
                    echo 'Compile'
                    sh'./mvnw clean compile'
                }
                post {
                    success {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: '239B56',
                        message: 'Step Compile OK - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                    unsuccessful {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: 'CB4335',
                        message: 'Step Compile KO - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                }
            }
            stage('Test') {
                steps {
                    echo 'Test'
                    sh'./mvnw test'
                }
                post {
                    always {
                        junit 'target/surefire-reports/*.xml'
                    }
                    success {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: '239B56',
                        message: 'Step Test OK - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                    unsuccessful {
                        // One or more steps need to be included within each condition's block.
                        office365ConnectorSend color: 'CB4335',
                        message: 'Step Test KO - Matthieu', 
                        webhookUrl: "$TEAMS_WEBHOOK"
                    }
                }
            }
            stage('quality') {
                steps {
                    withSonarQubeEnv('sonarqube') {
                        sh './mvnw sonar:sonar -Pcoverage'
                    }
                }
            }
            
        }
        post {
            success {
                // One or more steps need to be included within each condition's block.
                office365ConnectorSend color: '239B56',
                message: 'Job OK - Matthieu', 
                webhookUrl: "$TEAMS_WEBHOOK"
            }
            unsuccessful {
                // One or more steps need to be included within each condition's block.
                office365ConnectorSend color: 'CB4335',
                message: 'Job KO - Matthieu', 
                webhookUrl: "$TEAMS_WEBHOOK"
                
            }
        }

    }
}